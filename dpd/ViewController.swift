//
//  ViewController.swift
//  dpd
//
//  Created by Александр on 20.10.17.
//  Copyright © 2017 Александр. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var message: UILabel!

    @IBAction func btnPress(_ sender: Any) {
        message.text = "Hello world!"
    }
    @IBAction func btnDelete(_ sender: Any) {
        message.text = ""
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

